# Задание 5*
# Имеется текстовый файл с набором русских слов(имена существительные, им.падеж)
# Одна строка файла содержит одно слово.
# Написать программу которая выводит список слов, каждый элемент списка которого - это новое слово,
# которое состоит из двух сцепленных в одно, которые имеются в текстовом файле.
# Порядок вывода слов НЕ имеет значения
#
# Например, текстовый файл содержит слова: ласты, стык, стыковка, баласт, кабала, карась
#
# Пользователь вводмт первое слово: ласты
# Программа выводит:
# ластык
# ластыковка
#
# Пользователь вводмт первое слово: кабала
# Программа выводит:
# кабаласты
# кабаласт
#
# Пользователь вводмт первое слово: стыковка
# Программа выводит:
# стыковкабала
# стыковкарась


def find_concatenated_words(input_word, word_list):
    concatenated_words = []

    for word in word_list:
        for index_input_word, value_input_word in enumerate(input_word):
            if word == input_word:
                continue

            if input_word[index_input_word:] == word[:len(input_word[index_input_word:])]:
                concatenated_words.append(input_word[:index_input_word] + word)
    return concatenated_words


def proccess(input_word=input("Введите первое слово: "), filename="Russian_words.txt"):
    try:
        with open(filename, "r", encoding="utf-8") as file:
            words = [line.strip() for line in file]

        concatenated_words = find_concatenated_words(input_word, words)

        if concatenated_words:
            print("Результат:")
            for word in concatenated_words:
                print(word)
        else:
            print(f"В файле нет слов, которые модно было бы сцепить со словом: '{input_word}'.")

    except FileNotFoundError:
        print(f"Файл '{filename}' не найден.")


def main():
    proccess()


if __name__ == "__main__":
    main()
