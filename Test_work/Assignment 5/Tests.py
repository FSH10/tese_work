import unittest
from Assignment_5 import find_concatenated_words


class TestConcatenatedWords(unittest.TestCase):
    word_list = ['ласты', 'стык', 'стыковка', 'баласт', 'кабала', 'карась']

    def test_concatenated_words(self):
        input_word = 'ласты'
        result = find_concatenated_words(input_word, self.word_list)
        expected = ['ластык', 'ластыковка']
        self.assertCountEqual(result, expected)

        input_word = 'кабала'
        result = find_concatenated_words(input_word, self.word_list)
        expected = ['кабаласты', 'кабаласт']
        self.assertCountEqual(result, expected)

        input_word = 'стыковка'
        result = find_concatenated_words(input_word, self.word_list)
        expected = ['стыковкабала', 'стыковкарась']
        self.assertCountEqual(result, expected)

    def test_empty_result(self):
        input_word = 'vdeveevweq'
        result = find_concatenated_words(input_word, self.word_list)
        expected = []
        self.assertCountEqual(result, expected)

    def test_empty_input_word(self):
        input_word = ''
        result = find_concatenated_words(input_word, self.word_list)
        expected = []
        self.assertCountEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
