# Задание 4
# Имеется папка с файлами
# Реализовать удаление файлов старше N дней

import os
import datetime


def delete_old_files(folder_path, N):
    current_time = datetime.datetime.now()

    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)

        if os.path.isfile(file_path):
            file_madetime = datetime.datetime.fromtimestamp(os.path.getmtime(file_path))
            days_difference = (current_time - file_madetime).days

            if days_difference >= N:
                os.remove(file_path)
                print(f"Файл {file_name} удален, так как он старше {N} дней.")


def main():
    """Вам нужно поменять количество дней
        (в функции аргумент N) - это "возраст" вашего файла"""
    delete_old_files(folder_path="files", N=4)


if __name__ == "__main__":
    main()
