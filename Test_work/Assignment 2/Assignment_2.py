# Задание 2
# в наличии список множеств. внутри множества целые числа
# посчитать
#  1. общее количество чисел
#  2. общую сумму чисел
#  3. посчитать среднее значение
#  4. собрать все множества в один кортеж


m = [{11, 3, 5}, {2, 17, 87, 32}, {4, 44}, {24, 11, 9, 7, 8}]


def main():
    overall_nums_count = sum([len(i) for i in m])
    overall_nums_sum = sum(set().union(*m))
    mean_value = overall_nums_sum / overall_nums_count
    final_tuple = tuple(set().union(*m))

    print("Общее количество чисел:", overall_nums_count)
    print("Общая сумма чисел:", overall_nums_sum)
    print("Среднее значение:", mean_value)
    print("Все множества в одном кортеже:", final_tuple)


if __name__ == "__main__":
    main()
