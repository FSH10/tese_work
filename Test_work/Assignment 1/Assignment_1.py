# Задание 1
# имеется текстовый файл f.csv, по формату похожий на .csv с разделителем |

"""
lastname|name|patronymic|date_of_birth|id
Фамилия1|Имя1|Отчество1|21.11.1998|312040348-3048
Фамилия2|Имя2|Отчество2|11.01.1972|457865234-3431
...
"""

# добавлю еще несколько

"""
Фамилия3|Имя3|Отчество3|05.06.1985|598723401-1287
Фамилия4|Имя4|Отчество4|14.09.2000|789012345-5678
Фамилия5|Имя5|Отчество5|02.03.1978|123456789-9876
Фамилия6|Имя6|Отчество6|19.07.1995|457865234-3431
"""

# 1. Реализовать сбор уникальных записей
# 2. Случается, что под одиннаковым id присутствуют разные данные - найти такие записи


import csv


def read_csv(file_path):
    records = []
    with open(file_path, 'r', encoding='utf-8') as file:
        reader = csv.DictReader(file, delimiter='|')
        for row in reader:
            for pattern, items in row.items():
                classified_row = {}
                pattern_classification = pattern.split("|")
                values = items.split("|")
                if len(values) != len(pattern_classification):
                    raise IndexError(f"В документе: {file_path}, запись: {items} неверного формата ")
                for index in range(len(values)):
                    classified_row[pattern_classification[int(index)]] = values[int(index)].strip()
                records.append(classified_row)

    return records


def find_unique_records(records):
    unique_records = []
    seen_ids = set()

    for record in records:
        record_id = record['id']
        if record_id not in seen_ids:
            seen_ids.add(record_id)
            unique_records.append(record)

    return unique_records


def find_duplicated_ids(records):
    id_counts = {}
    duplicated_ids = set()

    for record in records:
        record_id = record['id']
        if record_id in id_counts:
            duplicated_ids.add(record_id)
        else:
            id_counts[record_id] = 1

    return duplicated_ids


def main(file_path='f.csv'):
    records = read_csv(file_path)
    unique_records = find_unique_records(records)
    print("Уникальные записи:")

    for record in unique_records:
        """Если Вы хотите получать запись в формате JSON, то нужно сохранить вывод простой record,
        если запись в формате - Фамилия1|Имя1|Отчество1|21.11.1998|312040348-3048, 
        то нужно заменить команду вывода на "|".join(record.values())"""
        # print("|".join(record.values()))
        print(record)

    duplicated_ids = find_duplicated_ids(records)
    print("\nЗаписпи с разными данными под одиннаковым id:")

    for record_id in duplicated_ids:
        duplicated_records = [record for record in records if record['id'] == record_id]
        print(f"ID: {record_id}, Записи: {duplicated_records}")


if __name__ == "__main__":
    main()
